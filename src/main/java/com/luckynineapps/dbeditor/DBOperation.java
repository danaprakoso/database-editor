package com.luckynineapps.dbeditor;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@WebServlet(name="DBOperation", value="/operate-db")
public class DBOperation extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    }

    public static String getDatabases() {
        String responseContent = "";
        try {
            Connection connection = DriverManager.getConnection(Constants.URL, "root", "BurgerLover123");
            ResultSet results = connection.createStatement().executeQuery("SHOW DATABASES;");
            while (results.next()) {
                String dbName = results.getString("database");
                responseContent += dbName;
                responseContent += ";";
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseContent += ("Exception: "+e.getMessage());
        }
        return responseContent;
    }

    public static String uploadDatabase() {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(Constants.URL, "root", "BurgerLover123");
            FileInputStream fis = new FileInputStream("hs.json");
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String content = "";
            String line;
            while ((line = br.readLine()) != null) {
                content += (line+",\n");
            }
            br.close();
            Statement statement = connection.createStatement();
            /*statement.addBatch("USE hs;");
            JSONArray data = new JSONArray(content);
            for (int i=0; i<data.length(); i++) {
                JSONObject obj = data.getJSONObject(i);
                String bk = obj.getString("bk");
                String bm = obj.getString("bm");
                String desc = obj.getString("desc");
                String descEn = obj.getString("desc_en");
                String id = obj.getString("id");
                String ppn = obj.getString("ppn");
                String ppnbm = obj.getString("ppnbm");
                System.out.println("BK: "+bk+", BM: "+bm+", Description (English): "+descEn);
                statement.addBatch("INSERT INTO (bk, bm, descr, descr_en, id, ppn, ppnbm)"
                        + "VALUES ("+bk+", "+bm+", "+desc+", "+descEn+", "+id+", "+ppn+", "+ppnbm+");");
            }*/
            ResultSet results = statement.executeQuery("SHOW DATABASES;");
            int i = 0;
            while (results.next()) {
                System.out.println("DB name: "+results.getString("database"));
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "<html><body><p>Hello, world</p></body></html>";
    }
}
