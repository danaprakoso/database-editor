<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.luckynineapps.dbeditor.DBOperation" %>
<html>
    <head>
    </head>
    <body>
        <a id="upload_db" href="#">Click here to upload database</a>
        <script type="text/javascript">

        	var myLink = document.getElementById("upload_db").onclick = function() {
        		document.write(<%= DBOperation.uploadDatabase() %>);
        	};
        </script>
    </body>
</html>